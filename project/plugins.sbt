resolvers += Resolver.bintrayRepo("oyvindberg", "converter")

resolvers ++= Seq(
  Resolver.sonatypeRepo("releases"),
  "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/",
  "Madoushi sbt-plugins" at "https://dl.bintray.com/madoushi/sbt-plugins/"
)

resolvers += Resolver.sonatypeRepo("releases")

addSbtPlugin("com.github.sbt" % "sbt-native-packager" % "1.9.16")

addSbtPlugin("com.typesafe.sbt" % "sbt-git" % "1.0.0")

addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.13")

addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.4.2")

addDependencyTreePlugin
