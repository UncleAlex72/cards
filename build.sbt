import com.typesafe.sbt.packager.docker.Cmd
import sbtrelease.ReleaseStateTransformations.*

val _scalaVersion = "3.6.2"
val mongoDbVersion = "4.0.2"
val pekkoVersion = "1.1.2"

lazy val server = project
  .in(file("."))
  .settings(
    name := "cards",
    scalaVersion := _scalaVersion,
    resolvers += "Atlassian public repository" at "https://packages.atlassian.com/maven-public/",
    libraryDependencies ++= Seq(
      "uk.co.unclealex" %% "mongodb-scala" % mongoDbVersion,
      "uk.co.unclealex" %% "futures" % "2.0.0",
      "ch.qos.logback" % "logback-classic" % "1.5.15",
      "uk.co.unclealex" %% "pekko-oidc-google" % "5.0.1",
      "org.apache.pekko" %% "pekko-actor-typed" % pekkoVersion,
      "org.apache.pekko" %% "pekko-slf4j" % pekkoVersion,
      "com.typesafe.scala-logging" %% "scala-logging" % "3.9.5",
      "commons-io" % "commons-io" % "2.18.0"
    ) ++ Seq(
      "docker-java-transport-httpclient5",
      "docker-java-core"
    ).map("com.github.docker-java" % _ % "3.2.13") ++ Seq(
      "org.scalatestplus" %% "mockito-4-2" % "3.2.11.0",
      "uk.co.unclealex" %% "mongodb-scala-test" % mongoDbVersion,
      "uk.co.unclealex" %% "thank-you-for-the-days" % "2.0.0",
    ).map(_ % "test"),
    dockerBaseImage := "adoptopenjdk/openjdk11-openj9:latest",
    dockerExposedPorts := Seq(9000),
    maintainer := "Alex Jones <alex.jones@unclealex.co.uk>",
    dockerRepository := Some("unclealex72"),
    packageName := "cards",
    dockerUpdateLatest := true,
    // Installing packages requires the run commands to be put high up in the list of docker commands
    Universal / javaOptions ++= Seq(
      "pidfile.path" -> "/dev/null",
      "java.awt.headless" -> "true"
    ).map { case (k, v) =>
      s"-D$k=$v"
    }
  )
  .enablePlugins(DockerPlugin, JavaAppPackaging)

// Releases

releaseProcess := Seq[ReleaseStep](
  releaseStepCommand("scalafmtCheckAll"),
  checkSnapshotDependencies, // : ReleaseStep
  inquireVersions, // : ReleaseStep
  runTest, // : ReleaseStep
  setReleaseVersion, // : ReleaseStep
  commitReleaseVersion, // : ReleaseStep, performs the initial git checks
  tagRelease, // : ReleaseStep
  releaseStepCommand(
    "stage"
  ), // : ReleaseStep, build server docker image.
  releaseStepCommand(
    "docker:publish"
  ), // : ReleaseStep, build server docker image.
  setNextVersion, // : ReleaseStep
  commitNextVersion, // : ReleaseStep
  pushChanges // : ReleaseStep, also checks that an upstream branch is properly configured
)
Global / onChangedBuildSource := ReloadOnSourceChanges
