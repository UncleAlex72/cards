package player

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import name.NameDao
import uk.co.unclealex.mongodb.ID

/** Persist and retrieve calls
  */
trait PlayerDao extends NameDao[Player]
