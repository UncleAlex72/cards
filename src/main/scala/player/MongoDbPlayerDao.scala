package player

import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Source
import name.MongoDbNameDao
import reactivemongo.api.DB
import uk.co.unclealex.mongodb.*

import java.time.Clock
import scala.concurrent.{ExecutionContext, Future}

/** An implementation of [[GameTypeDao]] that uses MongoDB
  *
  * @param ec
  *   The execution context for futures.
  */
class MongoDbPlayerDao(
    override val eventualDatabase: Future[DB],
    override val clock: Clock
)(implicit actorSystem: ActorSystem, ec: ExecutionContext)
    extends MongoDbNameDao[Player](eventualDatabase, clock, "players")
    with PlayerDao
