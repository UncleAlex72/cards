package player

import io.circe.Codec
import io.circe.generic.semiauto.*
import name.HasName
import reactivemongo.api.bson.{
  BSONDocumentHandler,
  BSONHandler,
  BSONObjectID,
  Macros
}
import uk.co.unclealex.mongodb.{ID, IsPersistable}
import uk.co.unclealex.stringlike.StringLikeJsonSupport

case class Player(
    _id: Option[ID],
    email: String,
    name: String,
    colour: String
) extends HasName

object Player extends StringLikeJsonSupport:

  given playerIsPersistable: IsPersistable[Player] = IsPersistable(
    getId = _._id,
    setId = id => _.copy(_id = Some(id))
  )

  given playerBsonHandler: BSONDocumentHandler[Player] = Macros.handler[Player]

  given playerCodec: Codec[Player] = deriveCodec[Player]
