package main

import com.typesafe.config.ConfigFactory
import controllers.ApiController
import game.*
import org.apache.pekko.actor.ActorSystem
import player.{MongoDbPlayerDao, PlayerDao}
import uk.co.unclealex.aog.RouteProvider
import uk.co.unclealex.aog.WebApp.*
import uk.co.unclealex.aog.security.UserInfoValidator
import uk.co.unclealex.mongodb.MongoDatabaseFactory

import java.time.{Clock, ZoneId}
import scala.concurrent.ExecutionContext

@main def cards(): Unit =
  val config = ConfigFactory.load()

  given actorSystem: ActorSystem = ActorSystem()
  given ec: ExecutionContext = actorSystem.dispatcher

  val clock =
    Clock.systemDefaultZone().withZone(ZoneId.of("Europe/London"))

  val (eventualCloseable, eventualDatabase) =
    MongoDatabaseFactory(config.getString("mongodb.url"))

  val gameDao: GameDao =
    new MongoDbGameDao(eventualDatabase = eventualDatabase, clock = clock)
  val gameTypeDao: GameTypeDao =
    new MongoDbGameTypeDao(eventualDatabase = eventualDatabase, clock = clock)
  val playerDao: PlayerDao =
    new MongoDbPlayerDao(eventualDatabase = eventualDatabase, clock = clock)
  val gameService: GameService = new GameServiceImpl(
    gameDao = gameDao,
    gameTypeDao = gameTypeDao,
    playerDao = playerDao,
    clock = clock
  )

  webApp
    .withUserInfoValidator(
      UserInfoValidator.fromSource(() => playerDao.findAll().map(_.email))
    )
    .withEventualCloseables(eventualCloseable)
    .serve: security =>
      new ApiController(
        gameService = gameService,
        gameDao = gameDao,
        gameTypeDao = gameTypeDao,
        playerDao = playerDao,
        security = security
      )
