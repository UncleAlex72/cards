package name

trait HasName {

  val name: String
}
