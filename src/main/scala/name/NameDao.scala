package name

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import name.HasName

/** Persist and retrieve calls
  */
trait NameDao[N <: HasName] {

  /** List all game types.
    */
  def findAll(): Source[N, NotUsed]

  def findByName(name: String): Source[N, NotUsed]
}
