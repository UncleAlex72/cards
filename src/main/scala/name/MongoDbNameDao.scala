package name

import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Source
import reactivemongo.api.DB
import reactivemongo.api.bson.BSONDocumentHandler
import reactivemongo.api.indexes.{Index, IndexType}
import uk.co.unclealex.mongodb.*

import java.time.Clock
import scala.concurrent.{ExecutionContext, Future}
import scala.reflect.ClassTag

/** An implementation of [[GameTypeDao]] that uses MongoDB
  *
  * @param ec
  *   The execution context for futures.
  */
abstract class MongoDbNameDao[N <: HasName: IsPersistable: ClassTag](
    override val eventualDatabase: Future[DB],
    override val clock: Clock,
    override val collectionName: String
)(using
    actorSystem: ActorSystem,
    ec: ExecutionContext,
    handler: BSONDocumentHandler[N]
) extends MongoDbDao[N](eventualDatabase, clock, collectionName) {

  override val indicies: Seq[Index.Default] =
    Seq(
      Index(
        Seq("name" -> IndexType.Ascending),
        Some("nameIndex"),
        unique = true
      )
    )

  override val defaultSort: Option[Sort] = "name".asc

  def findByName(name: String): Source[N, NotUsed] = findOne(
    "name" === name
  )
}
