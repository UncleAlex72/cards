package game

import org.apache.pekko.{Done, NotUsed}
import org.apache.pekko.stream.scaladsl.Source
import cats.data.ValidatedNec
import uk.co.unclealex.mongodb.ID

trait GameService {

  def insertGame(
      winner: String,
      gameType: String
  ): Source[ValidatedNec[String, Game], NotUsed]

  def removeGame(id: ID): Source[Done, NotUsed]

}
