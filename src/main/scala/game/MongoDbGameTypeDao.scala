package game

import org.apache.pekko.actor.ActorSystem
import name.MongoDbNameDao
import reactivemongo.api.DB

import java.time.Clock

import scala.concurrent.{ExecutionContext, Future}

/** An implementation of [[GameTypeDao]] that uses MongoDB
  *
  * @param ec
  *   The execution context for futures.
  */
class MongoDbGameTypeDao(
    override val eventualDatabase: Future[DB],
    override val clock: Clock
)(using actorSystem: ActorSystem, ec: ExecutionContext)
    extends MongoDbNameDao[GameType](eventualDatabase, clock, "gameTypes")
    with GameTypeDao
