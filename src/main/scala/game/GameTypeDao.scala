package game

import name.NameDao

/** Persist and retrieve calls
  */
trait GameTypeDao extends NameDao[GameType]
