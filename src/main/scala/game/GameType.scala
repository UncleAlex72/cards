package game

import io.circe.Codec
import io.circe.generic.semiauto.*
import name.HasName
import reactivemongo.api.bson.{BSONDocumentHandler, BSONHandler, Macros}
import uk.co.unclealex.mongodb.{ID, IsPersistable}
import uk.co.unclealex.stringlike.StringLikeJsonSupport

/** A type of card game.
  * @param name
  *   The name of the game.
  */
case class GameType(_id: Option[ID], name: String, icon: String) extends HasName

/** Json codecs for [[Game]]
  */
object GameType extends StringLikeJsonSupport:

  given gameTypeIsPersistable: IsPersistable[GameType] = IsPersistable(
    getId = _._id,
    setId = id => _.copy(_id = Some(id))
  )

  given gameTypeBsonHandler: BSONDocumentHandler[GameType] =
    Macros.handler[GameType]
  given gameTypeCodec: Codec[GameType] =
    deriveCodec[GameType]
