package game

import uk.co.unclealex.aog.Sources.required
import uk.co.unclealex.aog.Sources.headOption
import org.apache.pekko.stream.Materializer
import org.apache.pekko.stream.scaladsl.Source
import org.apache.pekko.{Done, NotUsed}
import cats.data.Validated.{Invalid, Valid}
import cats.data.ValidatedNec
import cats.implicits.{
  catsSyntaxTuple2Semigroupal,
  catsSyntaxValidatedIdBinCompat0
}
import uk.co.unclealex.mongodb.ID
import player.{Player, PlayerDao}

import java.time.Clock
import scala.concurrent.ExecutionContext

class GameServiceImpl(
    val gameDao: GameDao,
    val gameTypeDao: GameTypeDao,
    val playerDao: PlayerDao,
    val clock: Clock
)(implicit val ec: ExecutionContext, mat: Materializer)
    extends GameService {

  override def insertGame(
      winner: String,
      gameType: String
  ): Source[ValidatedNec[String, Game], NotUsed] = {
    val gameTypeSource = gameTypeDao
      .findByName(gameType)
      .required(s"$gameType is not a valid game type")
    val winnerSource =
      playerDao.findByName(winner).required(s"$winner is not a valid player")
    val gameTypeAndWinnerSource
        : Source[ValidatedNec[String, (GameType, Player)], NotUsed] =
      gameTypeSource.zipWith(winnerSource)((vg, vp) =>
        (vg, vp).mapN((g, p) => g -> p)
      )
    gameTypeAndWinnerSource.flatMapConcat {
      case Valid((gameType, winner)) =>
        gameDao
          .store(Game(None, clock.instant(), gameType.name, winner.name))
          .map(game => game.validNec)
      case Invalid(messages) => Source.single(Invalid(messages))
    }
  }

  override def removeGame(id: ID): Source[Done, NotUsed] = {
    gameDao.removeById(id).headOption.map(_ => Done)
  }

}
