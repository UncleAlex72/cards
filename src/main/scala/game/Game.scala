package game

import io.circe.Codec
import io.circe.generic.semiauto.*
import reactivemongo.api.bson.{BSONDocumentHandler, BSONHandler, Macros}
import uk.co.unclealex.mongodb.{ID, IsPersistable}
import uk.co.unclealex.stringlike.StringLike.*
import uk.co.unclealex.stringlike.{StringLike, StringLikeJsonSupport}

import java.time.format.DateTimeFormatter
import java.time.{Instant, OffsetDateTime}

/** A card game that has been won.
  * @param when
  *   The instant when the game was played.
  * @param gameType
  *   The type of game that was played.
  * @param winner
  *   The winner.
  */
case class Game(
    _id: Option[ID],
    when: Instant,
    gameType: String,
    winner: String
)

/** Json codecs for [[Game]]
  */
object Game extends StringLikeJsonSupport:

  given gameIsPersistable: IsPersistable[Game] = IsPersistable(
    getId = _._id,
    setId = id => _.copy(_id = Some(id))
  )

  given gameBsonHandler: BSONDocumentHandler[Game] = Macros.handler[Game]

  given gameCodec: Codec[Game] =
    given offsetDateTimeIsStringLike: StringLike[OffsetDateTime] =
      val formatter: DateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME
      fromFunctions(formatter.format, OffsetDateTime.parse(_, formatter))
    deriveCodec[Game]
