package game

import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Source
import reactivemongo.api.DB
import reactivemongo.api.indexes.{Index, IndexType}
import uk.co.unclealex.mongodb.*

import java.time.Clock
import scala.concurrent.{ExecutionContext, Future}

/** An implementation of [[GameDao]] that uses MongoDB
  *
  * @param ec
  *   The execution context for futures.
  */
class MongoDbGameDao(
    override val eventualDatabase: Future[DB],
    override val clock: Clock
)(using ec: ExecutionContext, actorSystem: ActorSystem)
    extends MongoDbDao[Game](eventualDatabase, clock, "games")
    with GameDao:

  override val indicies: Seq[Index.Default] =
    Seq("winner", "when", "gameType").map: columnName =>
      Index(Seq(columnName -> IndexType.Ascending), Some(s"${columnName}Index"))

  override val defaultSort: Option[Sort] = "when".asc
