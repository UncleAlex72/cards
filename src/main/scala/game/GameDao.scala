package game

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import uk.co.unclealex.mongodb.ID

import scala.concurrent.Future

/** Persist and retrieve calls
  */
trait GameDao {

  /** Insert a game
    *
    * @param game
    *   The game to insert.
    * @return
    *   A future containing either a list of errors or the newly persisted game.
    */
  def store(game: Game): Source[Game, NotUsed]

  def removeById(id: ID): Source[Int, NotUsed]

  def findAll(): Source[Game, NotUsed]
}
