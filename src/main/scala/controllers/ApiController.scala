package controllers

import com.github.pjfanning.pekkohttpcirce.FailFastCirceSupport
import game.{GameDao, GameService, GameTypeDao}
import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec
import io.circe.syntax.*
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.model.{
  HttpResponse,
  StatusCode,
  StatusCodes
}
import org.apache.pekko.http.scaladsl.server.{
  Directive,
  Directive0,
  Directives,
  Route
}
import player.PlayerDao
import uk.co.unclealex.aog.RouteProvider
import uk.co.unclealex.aog.directives.SourceDirectives
import uk.co.unclealex.aog.security.Security

import scala.concurrent.ExecutionContext

/** The main controller used for interacting with browsers.
  */
class ApiController(
    val gameService: GameService,
    val gameDao: GameDao,
    val gameTypeDao: GameTypeDao,
    val playerDao: PlayerDao,
    val security: Security
)(using actorSystem: ActorSystem, ec: ExecutionContext)
    extends RouteProvider
    with Directives
    with Routes
    with SourceDirectives
    with FailFastCirceSupport {

  import security.Directives.*

  private def getAt(p: String): Directive0 = get & path(p)

  override val route: Route = requireUser0:
    pathPrefix(cards / api):
      concat(
        path(games):
          get:
            listGames
          ~ post:
            registerGame
        ,
        getAt(players):
          listPlayers
        ,
        getAt(gameTypes):
          listGameTypes
      )

  private def registerGame: Route = {
    val statusCodeMapping: Map[StatusCode, StatusCode] = Map(
      StatusCodes.OK -> StatusCodes.Created,
      StatusCodes.NotFound -> StatusCodes.BadRequest
    )
    def useCreatedOrBadRequest(response: HttpResponse): HttpResponse = {
      statusCodeMapping.get(response.status).foldLeft(response) {
        _.withStatus(_)
      }
    }
    mapResponse(useCreatedOrBadRequest) {
      entity(as[GameResult]) { gameResult =>
        validatedSourceAsSingle(
          gameService.insertGame(
            winner = gameResult.winner,
            gameType = gameResult.gameType
          )
        )
      }
    }
  }

  private def listPlayers: Route = {
    complete(playerDao.findAll())
  }

  private def listGameTypes: Route = {
    complete(gameTypeDao.findAll())
  }
  private def listGames: Route = {
    complete(gameDao.findAll())
  }
}

case class GameResult(gameType: String, winner: String)

object GameResult:
  given gameResultCodec: Codec[GameResult] = deriveCodec[GameResult]
