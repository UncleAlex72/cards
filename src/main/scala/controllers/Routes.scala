package controllers

trait Routes {

  val cards = "cards"
  val api = "api"
  val games = "games"
  val gameTypes = "gameTypes"
  val players = "players"
}
