module.exports = {
  staticFileGlobs: [
    'build/static/css/**.css',
    'build/static/js/**.js',
    'build/static/media/**.*',
    'build/index.html',
    'build/logo.svg',
    'build/*.png',
    'build/site.webmanifest'
  ],
  swFilePath: './build/service-worker.js',
  stripPrefix: 'build/',
  importScripts: (['./service-worker-notifications.js']),
  runtimeCaching: [{
    urlPattern: '/cards/api/*',
    handler: 'networkFirst'
  }]
};
