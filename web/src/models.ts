import {Moment} from "moment";
import {Phase} from "./Phase";
import {List, Map} from "immutable";
import {RouterState} from "connected-react-router";
import {
    AuthenticationRequiredState,
    OfflineState,
    PushNotificationState,
    ServiceWorkerAlertsState
} from "react-redux-observable-offline-support";

export const ALL_GAMES = "all";

export interface UserInfo {
    readonly firstName?: string,
    readonly lastName?: string,
    readonly fullName?: string,
    readonly email?: string
}

export interface Player {
    readonly id: string,
    readonly name: string,
    readonly colour: string,
}

export interface GameType {
    readonly id: string,
    readonly name: string,
    readonly icon?: string,
}

export interface Game {
    readonly id: string,
    readonly gameType: string,
    readonly winner: string,
    readonly when: Moment
}

export interface NewGame {
    readonly winnerId: string,
    readonly gameTypeId: string
}

export interface Stats {
    readonly gameType?: string,
    readonly totalGames: number,
    readonly wins: Map<string, number>
}

export interface AppState {
    readonly phase: Phase,
    readonly userInfo?: UserInfo,
    readonly players?: Map<string, Player>,
    readonly gameTypes?: Map<string, GameType>,
    readonly stats?: Map<string, Stats>,
    readonly error?: any
}

export interface NewGameState {
    readonly selectedWinnerId?: string,
    readonly selectedGameTypeId?: string
}

export interface ToastState {
    readonly messages: List<string>
}

export interface State {
    readonly app: AppState,
    readonly newGame: NewGameState,
    readonly router: RouterState,
    readonly toast: ToastState,
    readonly serviceWorker: PushNotificationState,
    readonly serviceWorkerAlerts: ServiceWorkerAlertsState,
    readonly offline: OfflineState,
    readonly authenticationRequired: AuthenticationRequiredState
}
