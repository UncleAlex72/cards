import * as React from 'react';
import {Player, State, Stats} from "../models";
import {Seq} from "immutable";
import {
    Card,
    CardText,
    CardTitle,
    Cell,
    DataTable,
    Grid,
    TableBody,
    TableColumn,
    TableHeader,
    TableRow
} from "react-md";
import {connect} from 'react-redux'
import {getPlayers, getStatsForAllGames} from "../app";

export interface StateProps {
    readonly stats?: Stats,
    readonly players: Seq.Indexed<Player>
}

export type Props = StateProps

const Summary = ({stats, players}: Props) => {
    if (stats) {
        const playerSummaries = players.map(player => {
            const name = player.name;
            const wins = stats.wins.get(name, 0);
            return { name, wins };
        });
        return (
            <Grid>
                <Cell size={12} tabletSize={12} phoneSize={12}>
                    <Card className={"summary"}>
                        <CardTitle title={"Summary"}/>
                        <CardText>
                            <DataTable plain>
                                <TableHeader>
                                    <TableRow>
                                        <TableColumn>Player</TableColumn>
                                        <TableColumn>Wins</TableColumn>
                                    </TableRow>
                                </TableHeader>
                                <TableBody>
                                    {playerSummaries.toArray().map(summary => {
                                        return (
                                            <TableRow key={summary.name}>
                                                <TableColumn>{summary.name}</TableColumn>
                                                <TableColumn>{summary.wins}</TableColumn>
                                            </TableRow>
                                        )
                                    })}
                                </TableBody>
                            </DataTable>
                        </CardText>
                    </Card>
                </Cell>
            </Grid>
        );
    }
    else {
        return (<div/>)
    }
};

function mapStateToProps(state: State): StateProps {
    const stats = getStatsForAllGames(state);
    const players = getPlayers(state);
    return { stats, players}
}

export default connect(
    mapStateToProps
)(Summary);