import * as React from 'react';
import {GameType, Player, State} from "../models";
import {Seq} from "immutable";
import {Button, Card, CardActions, CardText, CardTitle, Cell, Grid, SelectField} from "react-md";
import {connect} from 'react-redux'
import {StateProps} from "../components/RegisterNewGame";
import {addNewGame, getGameTypes, getPhase, getPlayers, selectGameType, selectWinner} from "../app";
import {Phases} from "../Phase";
import {getSelectedGameTypeId, getSelectedWinnerId} from "../newGame";
import {Dispatch} from "redux";

export interface StateProps {
    readonly gameTypes: Seq.Indexed<GameType>,
    readonly players: Seq.Indexed<Player>,
    readonly disabled: boolean,
    readonly selectedWinnerId?: string,
    readonly selectedGameTypeId?: string
}

export interface DispatchProps {
    readonly onSubmit: (selectedPlayerId: string | undefined, selectedGameTypeId: string | undefined) => () => void,
    readonly onSelectGameType : (gameTypeId: string) => void,
    readonly onSelectWinner : (winnerId: string) => void
}

interface SelectFieldItem {
    readonly label: string,
    readonly value: string
}

function selectFieldAdapter<T extends { id: string, name: string }>(fields: Seq.Indexed<T>): SelectFieldItem[] {
    return fields.map( t => {
        return { label: t.name, value: t.id };
    }).toArray()
}

export type Props = DispatchProps & StateProps

const RegisterNewGame = ({
                             gameTypes,
                             players,
                             disabled,
                             selectedWinnerId,
                             selectedGameTypeId,
                             onSelectGameType,
                             onSelectWinner,
                             onSubmit}: Props) => {
    const buttonIsDisabled = !selectedWinnerId || !selectedGameTypeId || disabled;
    function changeAdapter(fn: (id: string) => void): (value: string | number) => void {
        return value => {
            if (typeof(value) === "string") {
                return fn(value);
            }
        }
    }
    return (
        <Grid>
            <Cell size={12} tabletSize={12} phoneSize={12}>
                <Card className={"register-new-game"}>
                    <CardTitle title={"Register New Game"}/>
                    <CardText>
                        <h4>Game</h4>
                        <SelectField
                            id="game-select-field"
                            placeholder="Game"
                            className="md-cell"
                            value={selectedGameTypeId || ""}
                            menuItems={selectFieldAdapter(gameTypes)}
                            position={SelectField.Positions.BELOW}
                            onChange={changeAdapter(onSelectGameType)}
                        />
                        <h4>Winner</h4>
                        <SelectField
                            id="player-select-field"
                            placeholder="Player"
                            className="md-cell"
                            value={selectedWinnerId || ""}
                            menuItems={selectFieldAdapter(players)}
                            position={SelectField.Positions.BELOW}
                            onChange={changeAdapter(onSelectWinner)}
                        />
                    </CardText>
                    <CardActions className={"right-hand-buttons"}>
                        <Button flat disabled={buttonIsDisabled} onClick={onSubmit(selectedWinnerId, selectedGameTypeId)}>
                            Submit
                        </Button>
                    </CardActions>
                </Card>
            </Cell>
        </Grid>
    );
};

function mapStateToProps(state: State): StateProps {
    const players = getPlayers(state);
    const gameTypes = getGameTypes(state);
    const disabled = getPhase(state) !== Phases.READY;
    const selectedWinnerId = getSelectedWinnerId(state);
    const selectedGameTypeId  = getSelectedGameTypeId(state);
    return {players, gameTypes, disabled, selectedGameTypeId, selectedWinnerId};
}

function mapDispatchToProps(dispatch: Dispatch): DispatchProps {
    function onSubmit(selectedPlayerId: string | undefined, selectedGameTypeId: string | undefined): () => void {
        return () => {
            if (selectedGameTypeId && selectedPlayerId) {
                dispatch(addNewGame({ winnerId: selectedPlayerId, gameTypeId: selectedGameTypeId }))
            }
        }
    }
    function onSelectGameType(gameId: string) {
        dispatch(selectGameType(gameId))
    }
    function onSelectWinner(winnerId: string) {
        dispatch(selectWinner(winnerId))
    }
    return { onSubmit, onSelectWinner, onSelectGameType };
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterNewGame);