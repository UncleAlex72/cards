import * as React from 'react';
import RegisterNewGame from "./RegisterNewGame";
import Summary from "./Summary";
import {State} from "../models";
import {isOnline} from "react-redux-observable-offline-support";
import {connect} from "react-redux";

export interface Props {
    readonly online: boolean
}

const NewGameScreen = ({online}: Props) => (
    <div>
        {online && <RegisterNewGame/>}
        <Summary/>
    </div>
);

function mapStateToProps(state: State): Props {
    const online = isOnline(state);
    return { online };
}
export default connect(mapStateToProps)(NewGameScreen);
