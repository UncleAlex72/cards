import * as React from 'react';
import {Phase, Phases} from "../Phase";
import {Route, Switch} from "react-router";
import NewGameScreen from "./NewGameScreen";
import Error from "./Error"
import {connect} from 'react-redux'
import {getError, getPhase} from "../app";
import {State} from "../models";
import ToastRack from "./ToastRack";
import Charts from "./Charts";
import Layout from "./Layout";
import {
    DisplayInitialiserProps,
    isAuthenticationRequired,
    OfflineSupportDetection
} from "react-redux-observable-offline-support";
import Authenticate from "./Authenticate";

export interface StateProps {
    readonly phase: Phase,
    readonly showError: boolean
    readonly showAuth: boolean
}

export type Props = StateProps & DisplayInitialiserProps

const App = ({phase, showError, showAuth, onDisplayInitialised}: Props) => {
    if (phase == Phases.INITIALISING) {
        return <div/>
    }
    else {
        onDisplayInitialised();
        if (showAuth) {
            return (<Authenticate/>)
        }
        else if (showError) {
            return (
                <Error>
                </Error>)
        }
        else {
            return (
                <Layout>
                    <Switch>
                        <Route path="/stats/:gameType" component={Charts} />
                        <Route path="/stats" exact component={Charts} />
                        <Route path="/" component={NewGameScreen} />
                    </Switch>
                    <ToastRack/>
                    <OfflineSupportDetection/>
                </Layout>
            );
        }
    }
};

function mapStateToProps(state: State): StateProps {
    const phase = getPhase(state);
    const showError = !!getError(state);
    const showAuth = isAuthenticationRequired(state);
    return {phase, showAuth, showError};
}

function mergeProps(stateProps: StateProps, _undefined: undefined, ownProps: DisplayInitialiserProps) {
    return {
        ...stateProps,
        ...ownProps
    }
}
export default connect(
    mapStateToProps,
    undefined,
    mergeProps
)(App);