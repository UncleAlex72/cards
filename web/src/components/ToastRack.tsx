import * as React from 'react';
import {List} from "immutable";
import {Snackbar} from "react-md";
import {connect} from 'react-redux'
import {State} from "../models";
import {Dispatch} from "redux";
import {dismissToast, getMessages} from "../toast";


export interface StateProps {
    readonly messages: List<string>
}

export interface DispatchProps {
    readonly onDismiss: () => void
}
export type Props = DispatchProps & StateProps

const ToastRack = ({messages, onDismiss}: Props) => {
    const realToasts = messages.map(text => {
        return { text }
    });
    return (
        <Snackbar
            id="toast-rack"
            toasts={realToasts.toArray()}
            autohide={true}
            autohideTimeout={3000}
            autoFocusAction
            onDismiss={onDismiss}
        />
    );
};

function mapStateToProps(state: State): StateProps {
    const messages = getMessages(state);
    return {messages};
}

function mapDispatchToProps(dispatch: Dispatch): DispatchProps {
    const onDismiss = () => {
        dispatch(dismissToast())
    };
    return {onDismiss}
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ToastRack);