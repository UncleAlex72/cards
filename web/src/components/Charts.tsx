import * as React from 'react';
import {Player, State, Stats} from "../models";
import {Seq} from "immutable";
import {Card, CardText, CardTitle, DataTable, TableBody, TableColumn, TableRow} from "react-md";
// @ts-ignore
import ReactSvgPieChart from "react-svg-piechart"
import {connect} from 'react-redux'
import {getGameType, getPlayers, getStatsForAllGames, getStatsForGameType} from "../app";

export interface Props {
    readonly stats?: Stats,
    readonly gameType?: string,
    readonly players?: Seq.Indexed<Player>
}

interface ChartData {
    readonly title: string,
    readonly value: number,
    readonly color: string
}

function chart(stats: Stats, players: Seq.Indexed<Player>): React.ReactNode {
    const empty: Seq.Indexed<ChartData> = Seq.Indexed();
    const chartData = stats.wins.toKeyedSeq().reduce((seq, value, title) => {
        const player = players.find(player => player.name === title);
        if (player) {
            const color = player.colour;
            const point: ChartData = { title, value, color };
            return seq.concat(point)
        }
        else {
            return seq;
        }
    }, empty).sortBy(cd => cd.title);
    console.log(chartData.toArray());
    return (
        <React.Fragment>
            <ReactSvgPieChart data={chartData.toArray()}/>
            {chartData.map(cd => {
                const {title, value, color} = cd;
                return (
                    <React.Fragment>
                        <h3>{title}</h3>
                        <DataTable plain>
                            <TableBody>
                                <TableRow>
                                    <TableColumn>Colour</TableColumn>
                                    <TableColumn>
                                        <div style={{height: "1em", width: "1em", backgroundColor: color}}>
                                            &nbsp;
                                        </div>
                                    </TableColumn>
                                </TableRow>
                                <TableRow>
                                    <TableColumn>Wins</TableColumn>
                                    <TableColumn>{value} ({(value / stats.totalGames * 100).toFixed(0)}%)</TableColumn>
                                </TableRow>
                            </TableBody>
                        </DataTable>
                    </React.Fragment>
                )
            })}
        </React.Fragment>
    );

}

const Charts = ({stats, gameType, players}: Props) => {
    const title = `Statistics for ${gameType || "all games"}`;
    function maybeChart(): React.ReactNode {
        if (!stats || stats.totalGames === 0 || !players || players.isEmpty()) {
            return <p>There is no data yet.</p>
        }
        else {
            return chart(stats, players)
        }
    }
    return (
        <Card className={"charts"}>
            <CardTitle title={title}/>
            <CardText>
                {maybeChart()}
            </CardText>
        </Card>
    )
};

function mapStateToProps(state: State): Props {
    const gameType: string | undefined = getGameType(state);
    const stats = (gameType ? getStatsForGameType(gameType) : getStatsForAllGames)(state);
    const players = getPlayers(state);
    return {gameType, stats, players};
}

export default connect(
    mapStateToProps
)(Charts);