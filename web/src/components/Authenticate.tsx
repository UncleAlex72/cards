import * as React from 'react';
import {connect} from 'react-redux'
import {Button, Card, CardActions, CardText, CardTitle} from "react-md";

export interface Props {
    readonly onAuthenticate: () => void
}

const Authenticate = ({onAuthenticate}: Props) => {
    return (
        <Card className={"authenticate-card"}>
            <CardTitle title={"Authentication required"}/>
            <CardText>
                Your session has expired. Please authenticate.
            </CardText>
            <CardActions centered={true}>
                <Button onClick={onAuthenticate} flat primary>Authenticate</Button>
            </CardActions>
        </Card>
    )
};

function mapDispatchToProps(): Props {
    const onAuthenticate = () => {
        window.location.href = "/cards/authenticate/google"
    };
    return {onAuthenticate};
}

export default connect(
    null, mapDispatchToProps
)(Authenticate);