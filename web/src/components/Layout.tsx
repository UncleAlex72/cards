import * as React from 'react';
import {FontIcon, ListItem, NavigationDrawer} from "react-md";
import {List, Seq} from "immutable";
import {GameType, State} from "../models";
import {connect} from 'react-redux'
import {Dispatch} from "redux";
import {RouterAction} from "connected-react-router"
import {getGameTypes, getName} from "../app";
import {routeToRegisterNewGame, routeToStats} from "../router";

export interface ChildrenProps {
    readonly children: React.ReactNode
}

export interface StateProps {
    readonly name: string,
    readonly gameTypes: Seq.Indexed<GameType>
}

export interface DispatchProps {
    readonly navigateToRegisterNewGame: () => void,
    readonly navigateToStats: (game?: string) => () => void
}

export type Props = DispatchProps & StateProps & ChildrenProps

type NavItem = {
    readonly key: string,
    readonly text: string,
    readonly icon?: string,
    readonly action: () => void} | null

const Layout = ({name, navigateToRegisterNewGame, gameTypes, children, navigateToStats}: Props) => {

    const navItems: List<NavItem> = List.of(
        {key: "home", text: "New Game", icon: "add_circle", action: navigateToRegisterNewGame},
        null
    ).concat(gameTypes.map(gameType => {
        return {key: gameType.id, text: gameType.name, icon: gameType.icon, action: navigateToStats(gameType.name) }
    })).concat(List.of(
        null,
        { key: "All", text: "All Games", icon: "group", action: navigateToStats() }
    ));
    function navItemAdapter(ni: NavItem) {
        if (ni !== null) {
            if (ni.icon) {
                const icon = <FontIcon>{ni.icon}</FontIcon>;
                return <ListItem key={ni.key} primaryText={ni.text} onClick={ni.action} leftIcon={icon}/>
            }
            else {
                return <ListItem key={ni.key} primaryText={ni.text} onClick={ni.action}/>
            }
        }
        else {
            return { divider: true }
        }
    }
    return (
        <NavigationDrawer
            toolbarTitle={name}
            navItems={navItems.map(navItemAdapter).toArray()}
            mobileDrawerType={NavigationDrawer.DrawerTypes.TEMPORARY}
            tabletDrawerType={NavigationDrawer.DrawerTypes.TEMPORARY}
            desktopDrawerType={NavigationDrawer.DrawerTypes.TEMPORARY}>
            {children}
        </NavigationDrawer>
    );
};


function mapStateToProps(state: State, childrenProps: ChildrenProps): StateProps & ChildrenProps {
    const name = getName(state);
    const children = childrenProps.children;
    const gameTypes = getGameTypes(state);
    return { name, children, gameTypes }
}

function mapDispatchToProps(dispatch: Dispatch): DispatchProps {
    function route(actionBuilder: () => RouterAction): () => void {
        return function() { dispatch(actionBuilder()); }
    }
    return {
        navigateToStats: (game?: string) => route(routeToStats(game)),
        navigateToRegisterNewGame: route(routeToRegisterNewGame)
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Layout);
