import {NewGameState, State} from "./models";
import {createSelector} from 'reselect'
import {AppAction, SelectGameTypeAction, SelectWinnerAction} from "./app";
import {Action, Reducer} from "redux";
import {actionExtractor, ReducerBuilder} from "react-redux-observable-offline-support";

export interface SelectGameTypeAction extends Action<string> {
    type: "SELECT_GAME_TYPE", id: string
}

export interface SelectWinnerAction extends Action<string> {
    type: "SELECT_WINNER", id: string
}

export interface ClearNewGameFormAction extends Action<string> {
    type: "CLEAR_NEW_GAME_FORM"
}

export type NewGameAction = SelectGameTypeAction | SelectWinnerAction | ClearNewGameFormAction

const isSelectGameType = actionExtractor<"SELECT_GAME_TYPE", SelectGameTypeAction>("SELECT_GAME_TYPE");

export function selectWinner(winnerId: string): SelectWinnerAction {
    return {
        type: "SELECT_WINNER",
        id: winnerId
    }
}

const isSelectWinner = actionExtractor<"SELECT_WINNER", SelectWinnerAction>("SELECT_WINNER");

export function clearNewGameForm(): ClearNewGameFormAction {
    return {
        type: "CLEAR_NEW_GAME_FORM"
    }
}

const isClearNewGameForm = actionExtractor<"CLEAR_NEW_GAME_FORM", ClearNewGameFormAction>("CLEAR_NEW_GAME_FORM");

export const newGame: Reducer<NewGameState, NewGameAction> = new ReducerBuilder({})
    .when(isSelectGameType, action => state => {
        const selectedGameTypeId = action.id;
        return {
            ...state,
            selectedGameTypeId
        }
    })
    .when(isSelectWinner, action => state => {
        const selectedWinnerId = action.id;
        return {
            ...state,
            selectedWinnerId
        }
    })
    .when(isClearNewGameForm, () => () => {
        return {}
    })
    .build();

const getState: (state: State) => NewGameState= state => state.newGame;

function getFromState<T>(extractor: (state: NewGameState) => T): (state: State) => T {
    return createSelector(getState, extractor)
}

export const getSelectedWinnerId: (state: State) => string | undefined =
    getFromState(state => state.selectedWinnerId);

export const getSelectedGameTypeId: (state: State) => string | undefined =
    getFromState(state => state.selectedGameTypeId);
