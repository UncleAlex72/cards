import {Phase, Phases} from './Phase'
import {ALL_GAMES, AppState, GameType, NewGame, Player, State, Stats, UserInfo} from "./models";
import {Observable, of, zip} from "rxjs";
import {Map, Seq} from "immutable";
import {catchError, flatMap, map, throwIfEmpty} from 'rxjs/operators';
import {combineEpics, Epic} from 'redux-observable';
import {parseAllStats, parseGame, parseGameTypes, parsePlayers, parseUserInfo} from "./parsers";
import {createSelector} from 'reselect'
import {concat} from "rxjs/internal/observable/concat";
import {findGameType} from "./router";
import {RouterState} from "connected-react-router";
import {Action, Reducer} from "redux";
import {
    actionExtractor,
    ErrorAwareEpicBuilder,
    errorIfNotValid,
    fetch$,
    installServiceWorker,
    listenFor,
    ReducerBuilder,
    requireAuthentication
} from "react-redux-observable-offline-support";
import {addToast} from "./toast";

export interface UpdatingAction extends Action<string> {
    type: "UPDATING",
    updating: boolean
}

export interface UpdatePlayersAction extends Action<string> {
    type: "UPDATE_PLAYERS", players: Map<string, Player>
}

export interface UpdateStatsAction extends Action<string> {
    type: "UPDATE_STATS", stats: Map<string, Stats>
}

export interface UpdateGameTypesAction extends Action<string> {
    type: "UPDATE_GAME_TYPES", gameTypes: Map<string, GameType>
}

export interface UpdateUserInfoAction extends Action<string> {
    type: "UPDATE_USER_INFO", userInfo: UserInfo
}

export interface AuthenticatedAction extends Action<string> {
    type: "AUTHENTICATED"
}

export interface ErroredAction extends Action<string> {
    type: "ERRORED", error: any
}

export interface AddNewGameAction extends Action<string> {
    type: "ADD_NEW_GAME",
    newGame: NewGame
}

export interface RequestStatsUpdateAction extends Action<string> {
    type: "REQUEST_STATS_UPDATE"
}

export interface InitialiseAction extends Action<string> {
    type: "INITIALISE"
}

export interface InitialisedAction extends Action<string> {
    type: "INITIALISED"
}

export interface SelectGameTypeAction extends Action<string> {
    type: "SELECT_GAME_TYPE", id: string
}

export interface SelectWinnerAction extends Action<string> {
    type: "SELECT_WINNER", id: string
}

export interface ClearNewGameFormAction extends Action<string> {
    type: "CLEAR_NEW_GAME_FORM"
}

export type AppAction =
    UpdatingAction |
    UpdateUserInfoAction |
    AuthenticatedAction |
    UpdatePlayersAction|
    UpdateGameTypesAction |
    UpdateStatsAction |
    ErroredAction |
    InitialiseAction |
    AddNewGameAction |
    RequestStatsUpdateAction |
    SelectGameTypeAction |
    SelectWinnerAction |
    ClearNewGameFormAction |
    InitialisedAction

export function updating(updating: boolean): UpdatingAction {
    return {
        type: "UPDATING",
        updating
    };
}

const isUpdating = actionExtractor<"UPDATING", UpdatingAction>("UPDATING");

export function updateUserInfo(userInfo: UserInfo): UpdateUserInfoAction {
    return {
        type: "UPDATE_USER_INFO",
        userInfo
    }
}

const isUpdateUserInfo = actionExtractor<"UPDATE_USER_INFO", UpdateUserInfoAction>("UPDATE_USER_INFO");

export function updatePlayers(players: Map<string, Player>): UpdatePlayersAction {
    return {
        type: "UPDATE_PLAYERS",
        players
    }
}

const isUpdatePlayers = actionExtractor<"UPDATE_PLAYERS", UpdatePlayersAction>("UPDATE_PLAYERS");

export function updateStats(stats: Map<string, Stats>): UpdateStatsAction {
    return {
        type: "UPDATE_STATS",
        stats
    }
}

const isUpdateStats = actionExtractor<"UPDATE_STATS", UpdateStatsAction>("UPDATE_STATS");

export function updateGameTypes(gameTypes: Map<string, GameType>): UpdateGameTypesAction {
    return {
        type: "UPDATE_GAME_TYPES",
        gameTypes
    }
}

const isUpdateGameTypes = actionExtractor<"UPDATE_GAME_TYPES", UpdateGameTypesAction>("UPDATE_GAME_TYPES");


export function addNewGame(newGame: NewGame): AddNewGameAction {
    return {
        type: "ADD_NEW_GAME",
        newGame
    }
}

const isAddNewGame = actionExtractor<"ADD_NEW_GAME", AddNewGameAction>("ADD_NEW_GAME");

export function requestStatsUpdate(): RequestStatsUpdateAction {
    return {
        type: "REQUEST_STATS_UPDATE"
    }
}

const isRequestStatsUpdate = actionExtractor<"REQUEST_STATS_UPDATE", RequestStatsUpdateAction>("REQUEST_STATS_UPDATE");

export function errored(error: any): ErroredAction {
    return {
        type: "ERRORED",
        error
    }
}

const isErrored = actionExtractor<"ERRORED", ErroredAction>("ERRORED");

export function initialise(): InitialiseAction {
    return {
        type: "INITIALISE"
    }
}

const isInitialise = actionExtractor<"INITIALISE", InitialiseAction>("INITIALISE");

export function initialised(): InitialisedAction {
    return {
        type: "INITIALISED"
    }
}

const isInitialised = actionExtractor<"INITIALISED", InitialisedAction>("INITIALISED");

export function selectGameType(gameTypeId: string): SelectGameTypeAction {
    return {
        type: "SELECT_GAME_TYPE",
        id: gameTypeId
    }
}

const isSelectGameType = actionExtractor<"SELECT_GAME_TYPE", SelectGameTypeAction>("SELECT_GAME_TYPE");

export function selectWinner(winnerId: string): SelectWinnerAction {
    return {
        type: "SELECT_WINNER",
        id: winnerId
    }
}

const isSelectWinner = actionExtractor<"SELECT_WINNER", SelectWinnerAction>("SELECT_WINNER");

export function clearNewGameForm(): ClearNewGameFormAction {
    return {
        type: "CLEAR_NEW_GAME_FORM"
    }
}

const isClearNewGameForm = actionExtractor<"CLEAR_NEW_GAME_FORM", ClearNewGameFormAction>("CLEAR_NEW_GAME_FORM");

export const initialState: AppState = { phase: Phases.INITIALISING  };

export const app: Reducer<AppState, AppAction> = new ReducerBuilder(initialState)
    .when(isInitialise, () => state => {
        return {
            ...state,
            phase: Phases.INITIALISING
        };
    })
    .when(isInitialised, () => state => {
        return {
            ...state,
            phase: Phases.READY
        }
    })
    .when(isErrored, action => state => {
        const error = action.error;
        return {
            ...state,
            error
        };
    })
    .when(isUpdateStats, action => state => {
        const stats = action.stats;
        return {
            ...state,
            stats
        };
    })
    .when(isUpdatePlayers, action => state => {
        const players = action.players;
        return {
            ...state,
            players
        };
    })
    .when(isUpdateGameTypes, action => state => {
        const gameTypes = action.gameTypes;
        return {
            ...state,
            gameTypes
        };
    })
    .when(isUpdateUserInfo, action => state => {
        const userInfo = action.userInfo;
        return {
            ...state,
            userInfo
        };
    })
    .when(isUpdating, action => state => {
        const phase = action.updating ? Phases.UPDATING : Phases.READY;
        return {
            ...state,
            phase
        };

    })
    .build();

const getState: (state: State) => AppState = state => state.app;

function getFromState<T>(extractor: (state: AppState) => T): (state: State) => T {
    return createSelector(getState, extractor)
}

export const getPhase: (state: State) => Phase = getFromState(state => state.phase);

export const getError: (state: State) => any = getFromState(state => state.error);

export const getUserInfo: (state: State) => UserInfo | undefined = getFromState(state => state.userInfo)
export const getName: (state: State) => string = createSelector(
    getUserInfo,
    (userInfo => (userInfo && (userInfo.firstName || userInfo.fullName || userInfo.lastName)) || "Somebody")
);

export const getGameTypes: (state: State) => Seq.Indexed<GameType> = getFromState(state =>
    (state.gameTypes || Seq.Indexed())
        .valueSeq()
        .sortBy(gameType => gameType.name)
);

export const getPlayers: (state: State) => Seq.Indexed<Player> = getFromState(state =>
    (state.players || Seq.Indexed())
        .valueSeq()
        .sortBy(player => player.name)
);

const getStats: (state: State) => Map<string, Stats> | undefined = getFromState(state => state.stats);

export const getStatsForGameType: (gameType: string) => (state: State) => Stats | undefined = gameType => createSelector(
    getStats,
    (statsByGameType?: Map<string, Stats>) => {
        if (statsByGameType) {
            return statsByGameType.get(gameType, undefined)
        }
        else {
            return undefined;
        }
    }
);

export const getStatsForAllGames: (state: State) => Stats | undefined = getStatsForGameType(ALL_GAMES);

export const getRouter: (state: State) => RouterState = (state: State) => state.router;
export const getGameType: (state: State) => string | undefined = createSelector(
    getRouter,
    (router) => findGameType(router)
);

function url(path: string) {
    return "/cards" + path;
}

const initialiseEpic: Epic = (action$, state) => action$.pipe(
    listenFor(isInitialise),
    flatMap(() => {
        return fetch$(url("/api/userinfo")).pipe(
            errorIfNotValid(),
            flatMap(response => response.json$()),
            map(parseUserInfo),
            flatMap(userInfo => {
                const players$: Observable<Map<string, Player>> = fetch$(url("/api/players")).pipe(
                    errorIfNotValid(),
                    flatMap(response => response.json$<object[]>()),
                    map(parsePlayers)
                );
                const gameTypes$: Observable<Map<string, GameType>> = fetch$(url("/api/gametypes")).pipe(
                    errorIfNotValid(),
                    flatMap(response => response.json$<object[]>()),
                    map(parseGameTypes)
                );
                const stats$: Observable<Map<string, Stats>> = fetch$(url("/api/stats")).pipe(
                    errorIfNotValid(),
                    flatMap(response => response.json$<object[]>()),
                    map(parseAllStats)
                );
                const zipped$ = zip(players$, gameTypes$, stats$);
                return zipped$.pipe(
                    flatMap(zipped => {
                        const [players, gameTypes, stats] = zipped;
                        return of(
                            updateUserInfo(userInfo),
                            updateGameTypes(gameTypes),
                            updatePlayers(players),
                            updateStats(stats),
                            initialised());
                    }),
                    throwIfEmpty(() => "no response from initialisation"));
            })
        );
    })
);

const initialisedEpic: Epic = (action$) => action$.pipe(
    listenFor(isInitialised),
    map(installServiceWorker)
);

const addNewGameEpic: Epic = (action$) => action$.pipe(
    listenFor(isAddNewGame),
    flatMap(action => {
        const body = JSON.stringify(action.newGame);
        const method = "POST";
        const headers: Headers = new Headers({"Content-Type": "application/json"});
        const stats$: Observable<Map<string, Stats>> =
            fetch$(url("/api/game"), { body, method, headers }).pipe(
                errorIfNotValid(),
                flatMap(response => response.json$()),
                map(parseGame),
                flatMap(() =>
                    fetch$(url("/api/stats")).pipe(
                        errorIfNotValid(),
                        flatMap(response => response.json$<object[]>()),
                        map(parseAllStats)
                    )
                )
            );
        return concat(
            of(updating(true)),
            stats$.pipe(
                flatMap(stats => of(
                    updateStats(stats), 
                    clearNewGameForm(), 
                    updating(false), 
                    addToast("Game added"))))
        ).pipe(
            catchError(err => {
                console.log("Adding a game failed");
                console.log(err);
                return of(addToast("Updating failed"))
            })
        );
    })
);

const errorAware = new ErrorAwareEpicBuilder()
    .onAuthenticationRequired(() => of(requireAuthentication(), initialised()))
    .onError((err: any) => of(initialised(), errored(err), updating(false)))
    .build();

export const rootEpic = errorAware(
    combineEpics(
        initialiseEpic,
        addNewGameEpic,
        initialisedEpic
    )
);
