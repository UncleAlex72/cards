const proxy = require('http-proxy-middleware');

module.exports = function(app) {
    ["api", "authenticate"].forEach(path => {
        app.use(proxy(`/cards/${path}`, {target: 'http://localhost:9000', xfwd: true}));
    });
};