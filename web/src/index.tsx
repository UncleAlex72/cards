import React from 'react';
import './index.scss';
import App from './components/App';
import {applyMiddleware, combineReducers, createStore} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import {app, initialise, rootEpic} from "./app";
import {Provider} from "react-redux";
import {combineEpics, createEpicMiddleware} from 'redux-observable';
import {createBrowserHistory} from 'history';
import {ConnectedRouter, connectRouter, routerMiddleware} from 'connected-react-router'
import {newGame} from "./newGame";
import {toast} from "./toast";
import {
    authenticationRequired,
    offline,
    serviceWorker,
    serviceWorkerAlerts,
    serviceWorkerEpic
} from "react-redux-observable-offline-support";
import {root} from "rxjs/internal-compatibility";
import {initialiseDisplay} from "react-redux-observable-offline-support/lib/initialiseDisplay";

const epicMiddleware = createEpicMiddleware();

const history = createBrowserHistory({basename: "/cards/"});
const router = connectRouter(history);

const store = createStore(
    combineReducers({
        router,
        app,
        newGame,
        toast,
        serviceWorker,
        serviceWorkerAlerts,
        offline,
        authenticationRequired
    }),
    {},
    composeWithDevTools(
        applyMiddleware(
            epicMiddleware,
            routerMiddleware(history)
        )
    )
);

const epicBuilder = (root: HTMLElement) => combineEpics(rootEpic, serviceWorkerEpic(root));
initialiseDisplay(
    store,
    epicMiddleware,
    "root",
    "loading",
    ipd => (
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <App onDisplayInitialised={ipd}/>
            </ConnectedRouter>
        </Provider>),
    epicBuilder,
    initialise()
);