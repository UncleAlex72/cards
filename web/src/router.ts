import {push, RouterAction, RouterState} from "connected-react-router"

export const routeToRegisterNewGame: () => RouterAction = () => {
    return push("/");
};

export const routeToStats: (game?: string) => () => RouterAction = game => () => {
    const suffix = game ? `/${game}` : "";
    return push(`/stats${suffix}`)
};

export function findGameType(router: RouterState): string | undefined {
    const match = /\/stats\/([a-zA-Z_0-9 ]+)$/g.exec(router.location.pathname);
    if (match) {
        return match[1];
    }
    else {
        return undefined;
    }
}