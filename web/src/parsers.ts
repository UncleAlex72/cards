import {ALL_GAMES, Game, GameType, Player, Stats, UserInfo} from "./models";
import {List, Map} from "immutable";
import moment, {Moment} from "moment";

function maybeParse<T>(fn: (o: object) => T): (o : object | undefined) => T | undefined {
    return function(o: object | undefined) {
      if (o === undefined) {
          return undefined;
      }
      else {
          return fn(o);
      }
    };
}

function parseWins(obj: object): Map<string, number> {
    const empty: Map<string, number> = Map();
    return Object.getOwnPropertyNames(obj).reduce((map, name) => {
        // @ts-ignore
        const value: number = obj[name];
        return map.set(name, value);
    }, empty)
}

function parseStats(obj: object): Stats {
    // @ts-ignore
    const { gameType, totalGames, wins } = obj;
    return { gameType, totalGames, wins: parseWins(wins) }
}

export function parseAllStats(objs: object[]): Map<string, Stats> {
    const allStats = objs.map(parseStats);
    const empty: Map<string, Stats> = Map();
    return allStats.reduce((map, stats) => {
        const key = stats.gameType || ALL_GAMES;
        return map.set(key, stats);
    }, empty);
}

function parsePlayer(obj: object): Player {
    // @ts-ignore
    const { _id, colour, name } = obj;
    return {
        id: _id,
        name,
        colour
    }
}

function parseGameType(obj: object): GameType {
    // @ts-ignore
    const { _id, icon, name } = obj;
    return {
        id: _id,
        name,
        icon
    }
}

export function parseUserInfo(obj: object): UserInfo {
    // @ts-ignore
    const {firstName, lastName, fullName, email} = obj;
    return {
        firstName,
        lastName,
        fullName,
        email
    }
}

export function parseGame(obj: object): Game {
    // @ts-ignore
    const {_id, winner, gameType, when} = obj;
    return {
        id: _id,
        winner,
        gameType,
        when: parseWhen(when)
    }
}
function parseWhen(when: string): Moment {
    return moment(when)
}

function parseAndMap<T extends {id : string}>(parser: (o: object) => T): (objs: object[]) => Map<string, T> {
    return objs => {
        function reducer(map: Map<string, T>, obj: object): Map<string, T> {
            const t = parser(obj);
            return map.set(t.id, t)
        }
        const empty: Map<string, T> = Map();
        return objs.reduce(reducer, empty);
    }
}

export const parsePlayers: (objs: object[]) => Map<string, Player> = parseAndMap(parsePlayer);

export const parseGameTypes: (objs: object[]) => Map<string, GameType> = parseAndMap(parseGameType);
