import {State, ToastState} from "./models";
import {List} from "immutable";
import {createSelector} from 'reselect'
import {Action, Reducer} from "redux";
import {actionExtractor, ReducerBuilder} from "react-redux-observable-offline-support";

export interface AddToastAction extends Action<string> { type: "ADD_TOAST", message: string }
export interface DismissToastAction extends Action<string> { type: "DISMISS_TOAST" }

export type ToastAction = AddToastAction | DismissToastAction

export function addToast(message: string): AddToastAction {
    return {
        type: "ADD_TOAST",
        message
    }
}

const isAddToast = actionExtractor<"ADD_TOAST", AddToastAction>("ADD_TOAST");

export function dismissToast(): DismissToastAction {
    return {
        type: "DISMISS_TOAST"
    }
}

const isDismissToast = actionExtractor<"DISMISS_TOAST", DismissToastAction>("DISMISS_TOAST");


export const initialState: ToastState = { messages: List.of() };

export const toast: Reducer<ToastState, ToastAction> = new ReducerBuilder(initialState)
    .when(isAddToast, action => state => {
        const messages = state.messages.push(action.message);
        return {
            ...state,
            messages
        }
    })
    .when(isDismissToast, action => state => {
        const messages = state.messages.delete(0);
        return {
            ...state,
            messages
        }
    })
    .build();

const getState: (state: State) => ToastState = state => state.toast;

function getFromState<T>(extractor: (state: ToastState) => T): (state: State) => T {
    return createSelector(getState, extractor)
}

export const getMessages: (state: State) => List<string> = getFromState(state => state.messages);
